# hse_login_app

A web login and authentication app.  Back-end in Node.js with Express framework.  Front-end in React.js.  Authentication by the Passport.js library. Property of McMaster - HSE (Health Systems Evidence).